package by.gsu.igi.lectures.lecture02;

public class Dog {
    String name;
    int age;

    public String toString() {
        return "Dog named " + name;
    }
}