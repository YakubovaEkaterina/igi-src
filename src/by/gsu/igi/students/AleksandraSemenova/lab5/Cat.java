package by.gsu.igi.students.AleksandraSemenova.lab5;

/**
 * Created by Aleksandra Semenova.
 */
public class Cat implements Speakable{
    @Override
    public void speak() {
        System.out.println("Mey-mey");
    }
}
