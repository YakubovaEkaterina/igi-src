package by.gsu.igi.students.DubinchikOlga.lab5;

/**
 * Created by DubinchikOlga.
 */
public interface Speakable {
    void speak();
}